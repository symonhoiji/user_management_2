/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.user_management;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Symon
 */
public class UserService {
    private static ArrayList<User> list = new ArrayList();
    public static void load(){
        System.out.println("Load");
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois= null;
        try {
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            list = (ArrayList<User>)ois.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    public static void save(ArrayList<User> list){
        System.out.println("Save");
        UserService.list = list;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        File file = null;
        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
                oos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static void addUser(String username,String password){
        User adduser = new User(username,password);
        list.add(adduser);
        save(list);
        load();
    }
    public static boolean auth(String loginName,String password){
        for(User user : list){
            
            if(user.getLoginName().equals(loginName) && user.getPassword().equals(password)) return true;
        }
        return false;
    }
    public static boolean checkUser(String loginName){
        for(User user:list){
            if(user.getLoginName().equals(loginName)) return true;
        }
        return false;
    }
    public static void getAllUser(){
        for(User user : list){
            System.out.println(user.toString());
        }
    }
    public static ArrayList<User> getListUser(){
        return list;
    }
}
